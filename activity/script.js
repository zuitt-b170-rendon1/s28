
// GET
fetch("https://jsonplaceholder.typicode.com/todos")
.then((response)=>response.json())
.then((json)=>console.log(json));


//GET
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response)=>response.json())
.then((json)=>console.log(json) + console.log(`The item ${json.title } on the list has a status of ${json.completed}`));

//POST
fetch("https://jsonplaceholder.typicode.com/todos",{
	method:"POST",
	headers: {
		"Content-Type": "application/json"
	},
	body:JSON.stringify({
		userId: 1,
		id: 201,
  		title: "Created To Do List Item",
  		completed: false
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// PUT
fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method:"PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body:JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structure ",
		id: 1,
		status: "Pending",
		title: "updated To Do List Item",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// PATCH
fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method:"PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body:JSON.stringify({
		status: "complete",
		dateCompleted: "07/09/21"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// DELETE
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE"
})